# miniconda

Install miniconda to manage Python

## Role Variables

* `home`
    * Type: String
    * Usages: Set miniconda home directory

* `installer`
    * Type: String
    * Usages: URL to installer download

```
miniconda:
  home: .config/miniconda
  installer: https://repo.continuum.io/miniconda/Miniconda3-latest-MacOSX-x86_64.sh
```
## Example Playbook

Including an example of how to use your role (for instance, with variables
passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - polkhan.miniconda

## License

MIT
